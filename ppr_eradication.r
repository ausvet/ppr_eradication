# PPR Eradication
# Animal movement network - disease eradication sequence optimisation
# Version:    1.2
# Author:     Angus Cameron
# Background: If disease eradication is undertaken progressively, breaking the population into small units
#             and animal movement used to prevent infection moving from the infected to the free areas,
#             the sequence of eradication can have a major impact on the number of animal movements that 
#             must be blocked.
# Purpose:    Calculate the best (or a 'good') sequence of progressive disease eradication from a list of population units
#             based on a movement matrix and movement rules, such that the number of blocked movements is minimised.

#-----------------------------
# Movement rules
#-----------------------------
# Set up the movement rules
# Matrix of Infected, Control, Free
# 1 = blocked, 0 = not blocked 
# Note Free -> Control may be permitted, but may require vaccination (so as not to dilute immunity)
rule <- array(c(
  0,1,1,
  0,1,1,
  0,0,0
), dim=c(3,3))

#-----------------------------
# Data - the movement matrix
#-----------------------------
# 1) Test data. A small 4*4 movement matrix to illustrate and test
# move <- array(c(
#    0,100,80,60,
#    2,  0,10,12,
#    4,  2, 0, 0,
#    3,  1, 0, 0), dim=c(4,4))
# dimen <- 4

# Real world data sets
# data is a CSV with no header, and is a simple integer matrix of the total movements between population units (network nodes)

# 2) Java goats 2016
# md <- read.csv('Java goats 2016.csv', header=FALSE)
# md <- read.csv('~/Dropbox (Ausvet)/AusVet team folder/Publications/PPR eradication/Java inter-district goat movements 2016.csv')#, header=FALSE)

# 3) New Zealand cattle and deer movements 2018
 md <- read.csv('NZ cattle 2018.csv', header=FALSE)

# find out how many units there are
dimen <- dim(md)[1]
# convert to an array
move <- array(as.numeric(unlist(md)),dim=c(dimen,dimen))

#-----------------------------
# Sequence of eradication
#-----------------------------
# Two options: 1) Exhausive list of all possible combinations
#                 Only feasible when number of units is < 10 (becuase it is factorial)
#              2) Sample from all possible seqences
#                 Doesn't find the best, but finds a good option
# Method 1
# get all the possible permutations (sequence of eradication)
# library(combinat)
# perms <- t(array(unlist(permn(dimen)),c(dimen,factorial(dimen))))

# Method 2
# Generate random sequences and test those
# 1000 takes < 15s; 100,000 takes about half an hour
nrs <- 1000
rand_seqs = array(0,c(nrs,dimen))
for (i in 1:nrs) {
  rand_seqs[i,] <- sample(1:dimen)
}
perms <- rand_seqs

#-----------------------------
# Define temporary variables
#-----------------------------
# block - 3D boolean array of whether to block a movement (1) or not (0)
#         Dimensions are eradication step, origin, destination
blockInit <- array(0,dim=c(dimen,dimen,dimen))

# control steps - array of status of each unit during each control step
#         Status: 1 Infected, 2 Control, 3 Free
#         Dimensions: Unit, control step
stepInit <-array(1,dim=c(dimen,dimen))
stepInit<- stepInit+upper.tri(stepInit,diag=FALSE) + upper.tri(stepInit,diag=TRUE)

#-----------------------------
# Main loop
#-----------------------------
# Loop through all defined sequences
# Calculate the total blocked
# Generate text progress bar
pb <- txtProgressBar(min = 0, max = dim(perms)[1], style = 3)

stepFun<-function(x) {
  (xMatrix[,x] %o% yMatrix[,x] ) * move
}

calcM<-function(p) {
  setTxtProgressBar(pb, p)
  step<-stepInit[perms[p,],]
  
  xMatrix<-ifelse(step>1,1,0)
  yMatrix<-ifelse(step<3,1,0)
  
  block<-blockInit
  for(steps in 1:dimen) {
    # reproduces rule
    block[steps,,]<-(xMatrix[,steps] %o% yMatrix[,steps] ) * move
    #stepFun(steps)
    
  }
  
  sum(block)
}

mSum<-sapply(1:nrs,FUN="calcM")
close(pb)

#-----------------------------
# Report the results
#-----------------------------
# create a data frame with the list of permutations and the total blocked for each permutation
res <- data.frame(perms,mSum)

print ('Best sequence')
head(res[order(mSum),], n=1)
# histogram of numbers blocked
windowsFonts(A = "Corbel")
hist(res$mSum, breaks=60, family="A", main="", xlab="Total blocked movements", 
     col="#A1BF64", border = "#8CAE48", xlim=c(0,max(res$mSum)), #xlim=c(min(res$mSum),max(res$mSum)), 
     xaxs="i", yaxs="i", cex.lab=1, cex.axis=1)

print (paste('Proportion of worst:', min(res$mSum)/max(res$mSum)))
print (paste('Density (proportion not zero):', sum(move!=0)/sum(move > -1)))
# full ordered listing
# res[order(mSum),]
